package com.farrascantona.jalin.repository;

import com.farrascantona.jalin.dto.CustomerDTO;
import com.farrascantona.jalin.entity.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CustomerRepository extends JpaRepository<Customer, String> {

    @Query("select new com.farrascantona.jalin.dto.CustomerDTO(c.id, c.name, c.phone) "
            + "from Customer c "
    )
    Page<CustomerDTO> findAllCustomer(Pageable pageable);
}
