package com.farrascantona.jalin.service;

import com.farrascantona.jalin.dto.CustomerDTO;
import com.farrascantona.jalin.entity.Customer;
import com.farrascantona.jalin.exception.CustomMessage;
import com.farrascantona.jalin.repository.CustomerRepository;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class CustomerService {
    Logger logger = LoggerFactory.getLogger(CustomerService.class);

    @Autowired
    private CustomerRepository customerRepository;

    @Transactional
    public ResponseEntity<CustomMessage> listAllCustomer(String page, String size) {
        try {
            System.out.println("[PRINT] Page: " + page + ", Size: " + size );
//            Pageable pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), Sort.by("createdDate").descending());
            Pageable pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
            Page<CustomerDTO> result = customerRepository.findAllCustomer(pageable);
            return new ResponseEntity<>(new CustomMessage("All customer data : ", false, result), HttpStatus.OK);
        } catch (Exception ex) {
            logger.trace("[LOG]ERROR!");
            ex.printStackTrace();
            String message = ex.getMessage();
            return new ResponseEntity<>(new CustomMessage("Warning! [" + message + "]", true, null), HttpStatus.BAD_REQUEST);
        }
    }

    @Transactional
    public ResponseEntity<CustomMessage> addNewOrEditCustomer(CustomerDTO request) {
        try {
            if (request.getId() == null && request.getName() != null) {
                System.out.println("[PRINT] request: " + request);
                Customer customer = new Customer();
                customer.setName(request.getName());
                customer.setPhone(request.getPhone());
                System.out.println("[PRINT] customer: " + customer);
                customerRepository.save(customer);
                return new ResponseEntity<>(new CustomMessage("Add new customer successfully!", false, customer), HttpStatus.OK);
            } else if(request.getId() != null){
                System.out.println("[PRINT] request: " + request);
                AtomicReference<String> message = new AtomicReference<>("Customer can't be find. Please try again!");
                Optional<Customer> optionalCustomer = customerRepository.findById(request.getId().toString());

                optionalCustomer.ifPresent(i -> {
                    System.out.println("[PRINT] i: " + i);
                    i.setName(request.getName());
                    i.setPhone(request.getPhone());
                    customerRepository.save(i);
                    System.out.println("[PRINT] i(NOW): " + i);
                    message.set("Edit customer successfully!");
                });

                return new ResponseEntity<>(new CustomMessage(message.get(), false, optionalCustomer), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new CustomMessage("We can't process your data. Please try again!", true, ""), HttpStatus.BAD_REQUEST);
            }
        } catch (Exception ex) {
            logger.trace("[LOG]ERROR!");
            ex.printStackTrace();
            String message = ex.getMessage();
            return new ResponseEntity<>(new CustomMessage("Warning! [" + message + "]", true, null), HttpStatus.BAD_REQUEST);
        }
    }

    @Transactional
    public ResponseEntity<CustomMessage> deleteCustomer(CustomerDTO request) {
        try {
            if(request.getId() != null){
                System.out.println("[PRINT] request: " + request);
                AtomicReference<String> message = new AtomicReference<>("Customer can't be find. Please try again!");
                Optional<Customer> optionalCustomer = customerRepository.findById(request.getId().toString());

                optionalCustomer.ifPresent(i -> {
                    customerRepository.delete(i);
                    message.set("Delete customer successfully!");
                });

                return new ResponseEntity<>(new CustomMessage(message.get(), false, optionalCustomer), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new CustomMessage("We can't process your data. Please try again!", true, ""), HttpStatus.BAD_REQUEST);
            }
        } catch (Exception ex) {
            logger.trace("[LOG]ERROR!");
            ex.printStackTrace();
            String message = ex.getMessage();
            return new ResponseEntity<>(new CustomMessage("Warning! [" + message + "]", true, null), HttpStatus.BAD_REQUEST);
        }
    }
}
