package com.farrascantona.jalin.controller;

import com.farrascantona.jalin.dto.CustomerDTO;
import com.farrascantona.jalin.exception.CustomMessage;
import com.farrascantona.jalin.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api/v1/customer")
@RestController
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping("/")
    public ResponseEntity<CustomMessage> hi(){
        return new ResponseEntity<CustomMessage>(new CustomMessage("HelloWorld", true, null), HttpStatus.OK);

    }

    @GetMapping("list")
    public ResponseEntity<CustomMessage> listAllCustomer(@RequestParam(value = "page", required = false, defaultValue = "0") String page,
                                                          @RequestParam(value = "size", required = false, defaultValue = "15") String size
    ) {
        System.out.println("[PRINT] listAllCustomer..");
        return customerService.listAllCustomer(page, size);
    }

    @PostMapping("add")
    public ResponseEntity<CustomMessage> addNewCustomer(@RequestBody CustomerDTO request) {
        System.out.println("[PRINT] addNewCustomer..");
        return customerService.addNewOrEditCustomer(request);
    }

    @PatchMapping("edit")
    public ResponseEntity<CustomMessage> editCustomer(@RequestBody CustomerDTO request) {
        System.out.println("[PRINT] editCustomer..");
        return customerService.addNewOrEditCustomer(request);
    }

    @DeleteMapping("delete")
    public ResponseEntity<CustomMessage> deleteCustomer(@RequestBody CustomerDTO request) {
        System.out.println("[PRINT] deleteCustomer..");
        return customerService.deleteCustomer(request);
    }
}
