package com.farrascantona.jalin.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import java.math.BigInteger;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "customer")
public class Customer {

    @Id
//    @GeneratedValue(generator = "uuid")
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @GeneratedValue( generator = "increment" )
    @GenericGenerator( name = "increment", strategy = "increment" )
    private BigInteger id;

    private String name;
    private String phone;
}
