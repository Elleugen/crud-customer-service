package com.farrascantona.jalin.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigInteger;
import java.time.LocalDateTime;
@Data
//@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CustomerDTO {

    private BigInteger id;
    private String name;
    private String phone;


    public CustomerDTO(BigInteger id, String name, String phone) {
        this.id = id;
        this.name = name;
        this.phone = phone;
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
